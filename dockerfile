FROM python:3

EXPOSE 5001

RUN mkdir /user_management
WORKDIR /user_management

COPY ./requirements.txt /user_management/requirements_user.txt
RUN pip install -r requirements_user.txt

COPY . /user_management
CMD ["python", "user_management.py"]