from flask import Flask, Response
import mysql.connector


app = Flask(__name__)

app.config['SECRET_KEY'] = "thisissecret"

#connect to the database
def connect_db():
    mysql_config = {
        'user': 'root',
        'password': 'root',
        'host': 'user_db',
        'database': 'users'
    }

 #connects to the sql
    connected = False
    while connected is False:
        try:
            con = mysql.connector.connect(**mysql_config)
            connected = True

        except Exception as e:
            continue

    return con


#For authentication we need a User model class that will store the username, password etc associated with a user.
class User(object):

    def __init__(self, username, lastname ,email , password ):
        self.username = username
        self.lastname = lastname
        self.password = password
        self.email = email

    @property
    def __repr__(self):
        return "%d/%s/%s/%s/%s" % (self.username, self.lastname, self.password, self.email)






    if __name__ == "__main__":
        app.run()